/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vrc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author Rednaz
 */
public class VRC {

    /**
     * @param args the command line arguments
     */
    
    public static Scanner in = new Scanner(System.in);
    public static int choice = 0;
    public static int counter = 0;
    public static Visitor myV[] = new Visitor[50];
    public static Calendar cal = Calendar.getInstance();
    public static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        do
        {
            System.out.println("Welcome to the Veteran's Resource Center!");
            System.out.println("1)User or 2)Admin?");
            System.out.println("(0 exits)");
            choice = in.nextInt();

            while (choice < 0 || choice > 2)
            {
                System.out.println("Incorrect Input");
                System.out.println("1)User or 2)Admin?");
                System.out.println("(0 exits)");
                choice = in.nextInt();
            }

            switch(choice)
            {
                case 1: user(); break;
                case 2: admin(); break;
            }
        } while (choice!=0);
    }
    
    public static void user()
    {
        String fName = new String();
        String lName = new String();

        System.out.println("Please enter your first name");
        fName = in.next();
        System.out.println("Please enter your last name");
        lName = in.next();

        System.out.println("Why are you visiting today?");
        System.out.println("1) Discuss a Personal Matter");
        System.out.println("2) Microwave");
        System.out.println("3) PC for email or homework");
        System.out.println("4) Book Donation");
        choice = in.nextInt();

        while (choice < 1 || choice > 4)
        {
            System.out.println("Incorrect Input");
            System.out.println("Why are you visiting today?");
            System.out.println("1) Discuss a Personal Matter");
            System.out.println("2) Microwave");
            System.out.println("3) PC for email or homework");
            System.out.println("4) Book Donation");
            choice = in.nextInt();
        }

        myV[counter] = new Visitor();
        myV[counter].setfName(fName);
        myV[counter].setlName(lName);
        myV[counter].setPurpose(choice);  
        myV[counter].setTime(sdf.format(cal.getTime()));
        
        counter++;
    }
    
    public static void admin()
    {
        int adminCounter = 0;
        
        System.out.println("Printing Visitors:\n");
        System.out.println("Name\t\tReason for Visit\tTime of Visit");
        
        while (myV[adminCounter] != null)
        {
            System.out.print( myV[adminCounter].getfName() + " " + 
                                myV[adminCounter].getlName() + "\t");
            switch(myV[adminCounter].getPurpose())
            {
                case 1: System.out.print("Discussed a Personal matter"); break;
                case 2: System.out.print("Microwave"); break;
                case 3: System.out.print("Computer"); break;
                case 4: System.out.print("Book Donation"); break;
            }
            System.out.print("\t" + myV[adminCounter].getTime() + "\n");
            adminCounter++;
        }
        
        System.out.println("\n");
    }
}