/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vrc;

import java.io.*;

/**
 *
 * @author Rednaz
 */
public class VRC_Out {
    
    private File myFile;
    private int counter = 0;
    
    VRC_Out()
    {
        
    }
    
    VRC_Out(Visitor[] myV)
    {
        this.myFile = new File("VRC.txt");
        
        try
        {
            if (this.myFile.exists() == false)
            {
                System.out.println("\tMaking new file");
                this.myFile.createNewFile();
            }
            
            FileWriter out = new FileWriter(this.myFile, true);
            
            while(myV[this.counter] != null)
            {
                out.append( myV[this.counter].getfName() + " " + 
                            myV[this.counter].getlName() + " ");
                        
                switch(myV[this.counter].getPurpose())
                {
                    case 1: out.append("Discussed a Personal matter "); break;
                    case 2: out.append("Microwave "); break;
                    case 3: out.append("Computer "); break;
                    case 4: out.append("Book Donation "); break;
                }
                out.append( myV[this.counter].getTime() + "\n");
                
                this.counter++;
            }
            
            out.close();
        }
        catch(IOException e)
        {
            System.out.println("Could not create File: " + e);
        }
    }
    
}