/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vrc;

/**
 *
 * @author Rednaz
 */
public class Visitor {
    
    // private variables
    private String fName;
    private String lName;
    private int purpose;
    private String time;
    
    // empty constructor
    Visitor()
    {
        
    }
    
    // base set functions
    public void setfName(String fName)
    {
        this.fName = fName;
    }
    public void setlName(String lName)
    {
        this.lName = lName;
    }
    public void setPurpose(int purpose)
    {
        this.purpose = purpose;
    }
    public void setTime(String time)
    {
        this.time = time;
    }
    
    // base get functions 
    public String getfName()
    {
        return this.fName;
    }
    public String getlName()
    {
        return this.lName;
    }
    public int getPurpose()
    {
        return this.purpose;
    }
    public String getTime()
    {
        return this.time;
    }
}
